module.exports = {
  authConfig: require('./auth.config'),
  databaseConfig: require('./database.config'),
  imdbConfig: require('./imdb.config'),
  serverConfig: require('./server.conf')
}
