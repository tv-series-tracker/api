module.exports = {
    secret: process.env.AUTH_JWT_SECRET,
    expires: process.env.AUTH_JWT_EXPIRES,
    rounds: process.env.AUTH_JWT_ROUNDS,
}
