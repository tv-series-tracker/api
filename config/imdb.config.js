module.exports = {
    IMDB_API_KEY: process.env.IMDB_API_KEY,
    IMDB_BASE_URL: process.env.IMDB_BASE_URL,
}
