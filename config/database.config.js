require('dotenv').config()
module.exports = {
  username: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
  port: process.env.DB_PORT,
  host: process.env.DB_HOST,
  dialect: 'mysql',
  logging: process.env.NODE_ENV === 'development' ? console.log : false,
  seederStorage: 'sequelize',
  seederStorageTableName: 'seeds',
  migrationStorage: 'sequelize',
  migrationStorageTableName: 'migrations'
}
