require('dotenv').config()
const app = require('./app.js')
const { sequelize } = require('./models/index')
const { SERVER_PORT } = require('../config/server.conf')

app.listen(SERVER_PORT, () => {
  console.log(`Server started at PORT ${SERVER_PORT}`)
  sequelize.authenticate().then(() => {
    console.log('database connnected')
  })
})
