const express = require('express')
const { authController } = require('../controllers')
const router = express.Router()

// login and register
router.post('/api/auth/signin', authController.signIn)
router.post('/api/auth/signin', authController.signUp)

module.exports = router
