module.exports = {
  authRoutes: require('./auth.routes'),
  seriesRoutes: require('./series.routes'),
  userSeries: require('./userSeries.routes')
}
