const express = require('express')
const { serieController } = require('../controllers')
const { authMiddleware } = require('../middlewares')
const router = express.Router()

router.get('/api/series/search', authMiddleware.auth, serieController.searchSeries)
module.exports = router
