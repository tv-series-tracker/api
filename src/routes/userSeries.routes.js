const express = require('express')
const { userSerieController } = require('../controllers')
const { authMiddleware } = require('../middlewares')
const router = express.Router()

router.post('/api/user-series', authMiddleware.auth, userSerieController.addSeriesToUser)
router.get('/api/user-series', authMiddleware.auth, userSerieController.listSeriesByUser)
module.exports = router
