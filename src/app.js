const express = require('express')
const morgan = require('morgan')
const { authRoutes, seriesRoutes, userSeries } = require('./routes')
const app = express()

if (process.env.NODE_ENV === 'development') {
  app.use(morgan('dev'))
  // Listener for unhandled exceptions
  process.on('uncaughtException', (error) => {
    console.error('unhandled exception:', error)
    process.exit(1)
  })
}

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(authRoutes, seriesRoutes, userSeries)

module.exports = app
