module.exports = {
  seriesServices: require('./serie.services'),
  imdbServices: require('./imdb.services'),
  userSerieService: require('./userSerie.services'),
  userService: require('./user.services')
}
