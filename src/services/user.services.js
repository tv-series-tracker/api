const { User } = require('../models/index')
const bcrypt = require('bcrypt')
const Jwt = require('jsonwebtoken')
const { authConfig } = require('../../config')
const { serialize } = require('cookie')

async function createUser (userParams) {
  const salt = bcrypt.genSaltSync(10)
  const password = bcrypt.hashSync(userParams.password, salt)
  const newUser = { ...userParams, password }
  const userCreated = await User.create(newUser)
  return {
    name: userCreated.name,
    email: userCreated.email
  }
}
async function authenticateUser (userParams) {
  const userFound = await User.findOne({
    where: {
      email: userParams.email
    }
  })

  if (!userFound) {
    return { success: false, status: 404, message: 'Invalid credentials' }
  }

  const isValidPassword = await bcrypt.compare(
    userParams.password,
    userFound.password
  )

  if (!isValidPassword) {
    return { success: false, status: 401, message: 'Invalid credentials' }
  }

  const jwtPayload = { email: userParams.email, iduser: userFound.id }

  const token = Jwt.sign(jwtPayload, authConfig.secret, {
    expiresIn: authConfig.expires
  })
  const serializedToken = serialize('tokenCook', token, {
    httpOnly: true,
    secure: process.env.NODE_ENV === 'production',
    sameSite: 'none',
    maxAge: 7200,
    path: '/'
  })

  return { success: true, serializedToken }
}

module.exports = { createUser, authenticateUser }
