const { Serie } = require('../models')
const save = async (serie) => await Serie.create(serie)

module.exports = { save }
