const axios = require('axios')
const imdbConfig = require('../../config/imdb.config')
const BASE_URL = imdbConfig.IMDB_BASE_URL
const API_KEY = imdbConfig.IMDB_API_KEY

const searchSeries = async (keyword) => {
  const { data } = await axios.get(
        `${BASE_URL}/SearchSeries/${API_KEY}/${keyword}`
  )
  return data
}
const serieFullDetail = async (serieId) => {
  const { data } = await axios.get(
        `${BASE_URL}/Title/${API_KEY}/${serieId}/FullSeries`
  )
  return data
}

module.exports = {
  searchSeries,
  serieFullDetail
}
