const { UserSerie, Serie } = require('../models')

const getUserSeriesByUserid = async (userId) => await UserSerie.findAll({
  where: {
    user_id: userId
  },
  include: [Serie]
})

module.exports = { getUserSeriesByUserid }
