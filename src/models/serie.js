'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class Serie extends Model {
    static associate (models) {
      Serie.hasMany(models.UserSerie, { foreignKey: 'serie_id' })
    }
  }
  Serie.init(
    {
      code: {
        type: DataTypes.STRING,
        unique: true
      },
      image: DataTypes.STRING,
      title: DataTypes.STRING,
      resultType: DataTypes.STRING,
      description: DataTypes.STRING,
      seasons: {
        type: DataTypes.ARRAY(DataTypes.INTEGER),
        defaultValue: []
      }
    },
    {
      sequelize,
      modelName: 'Serie',
      validate: {
        codeAreUnique: async function () {
          const serieSameCode = await Serie.findOne({
            where: { code: this.code }
          })

          if (serieSameCode) {
            if (serieSameCode.code === this.code) {
              throw new Error(
                'The series you are trying to store is already createde'
              )
            }
          }
        }
      }
    }
  )
  return Serie
}
