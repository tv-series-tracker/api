'use strict'
const { Model } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class UserSerie extends Model {
    static associate (models) {
      UserSerie.belongsTo(models.User, { foreignKey: 'user_id' })
      UserSerie.belongsTo(models.Serie, { foreignKey: 'serie_id' })
    }
  }
  UserSerie.init(
    {
      user_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        references: {
          model: 'users',
          key: 'id'
        }
      },
      serie_id: {
        type: DataTypes.INTEGER,
        primaryKey: true,
        references: {
          model: 'series',
          key: 'id'
        }
      }
    },
    {
      sequelize,
      modelName: 'UserSerie'
    }
  )
  return UserSerie
}
