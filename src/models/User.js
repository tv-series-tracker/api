'use strict'
const { Model, Op, Sequelize } = require('sequelize')
module.exports = (sequelize, DataTypes) => {
  class User extends Model {
    static associate (models) {
      User.hasMany(models.UserSerie, { foreignKey: 'user_id' })
    }

    async validate (options) {
      try {
        return await super.validate(options)
      } catch (err) {
        if (err instanceof Sequelize.ValidationError) {
          throw new Sequelize.ValidationError(
            err.errors.map((error) => error.message.replace('Validation error: ', '')),
            {
              model: this,
              prefix: ''
            }
          )
        } else {
          throw err
        }
      }
    }
  }
  User.init(
    {
      name: {
        type: DataTypes.STRING,
        allowNull: false
      },
      password: {
        type: DataTypes.STRING,
        allowNull: false
      },
      email: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true,
        validate: {
          isEmail: {
            args: true,
            msg: 'The email must be valid'
          }
        }
      },
      username: {
        type: DataTypes.STRING,
        allowNull: false,
        unique: true
      }
    },
    {
      sequelize,
      modelName: 'User',
      validate: {
        emailAndUsernameAreUnique: async function () {
          const userWithSameEmailOrUsername = await User.findOne({
            where: {
              [Op.or]: [{ email: this.email }, { username: this.username }]
            }
          })

          if (userWithSameEmailOrUsername) {
            if (userWithSameEmailOrUsername.email === this.email) {
              throw new Error('Email address is already in use')
            } else {
              throw new Error(
                'El nombre de usuario ya está en usoThe username is already taken'
              )
            }
          }
        }
      }
    }
  )
  return User
}
