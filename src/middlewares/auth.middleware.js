const cookie = require('cookie')
const Jwt = require('jsonwebtoken')
const { authConfig } = require('../../config')

const auth = async (req, res, next) => {
  try {
    const cookies = req.headers.cookie ? cookie.parse(req.headers.cookie) : {}
    const token = cookies.tokenCook
    if (!token) {
      return res.status(401).json({ msg: 'Access denied' })
    } else {
      Jwt.verify(token, authConfig.secret, (err, decodedToken) => {
        if (err) res.status(401).json({ error: false, mesage: 'token verification failed' })
        else next()
      })
    }
  } catch (error) {
    res.status(500).json({ error: false, message: error.mesage })
  }
}

module.exports = {
  auth
}
