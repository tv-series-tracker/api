const { imdbServices } = require('../services')
const Joi = require('joi')

const searchSeries = async (req, res) => {
  try {
    const schema = Joi.object({
      keyword: Joi.string().required()
    }).options({ stripUnknown: true })
    const { error, value } = schema.validate(req.query, {
      abortEarly: false
    })
    if (error) {
      const message = error.details.map((d) => d.message).join(', ')
      return res.status(400).json({ success: false, message })
    }

    const data = await imdbServices.searchSeries(value.keyword)

    if (data.errorMessage) { return res.status(400).json({ success: false, message: 'Bad Request' }) }

    res.json({ success: true, data: data.results })
  } catch (error) {
    res.status(500).json({ success: false, message: error.message })
  }
}

module.exports = { searchSeries }
