module.exports = {
  authController: require('./auth.controller'),
  serieController: require('./serie.controller'),
  userSerieController: require('./userSerie.controller')
}
