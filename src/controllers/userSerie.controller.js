const { imdbServices, seriesServices, userSerieService } = require('../services')
const Joi = require('joi')

const listSeriesByUser = async (req, res) => {
  try {
    const data = await userSerieService.getUserSeriesByUserid(1)
    res.json({ success: true, data })
  } catch (error) {
    console.log(error.stack)
    res.status(500).json({ success: false, message: error.message })
  }
}

const addSeriesToUser = async (req, res) => {
  try {
    const schema = Joi.object({
      id: Joi.string().required(),
      image: Joi.string(),
      title: Joi.string().required(),
      description: Joi.string().required(),
      resultType: Joi.string().required()
    }).options({ stripUnknown: true })

    const { error, value } = schema.validate(req.body, {
      abortEarly: false
    })

    if (error) {
      const message = error.details.map((d) => d.message).join(', ')
      return res.status(400).json({ success: false, message })
    }

    const serieFullDetail = await imdbServices.serieFullDetail(value.id)
    const seasons = serieFullDetail?.tvSeriesInfo?.seasons ?? []
    const serieCreated = await seriesServices.save({
      code: value.id,
      image: value.image,
      title: value.title,
      resultType: value.resultType,
      description: value.description,
      seasons
    })

    res.json({
      success: true,
      message: 'Serie created successfully',
      data: serieCreated
    })
  } catch (error) {
    res.status(500).json({ success: false, message: error.message })
  }
}

module.exports = { listSeriesByUser, addSeriesToUser }
