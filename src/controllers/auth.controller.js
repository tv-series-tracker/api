const { userService } = require('../services')
const Joi = require('joi')

const signUp = async (req, res) => {
  try {
    const schema = Joi.object({
      username: Joi.string().required(),
      password: Joi.string().required(),
      name: Joi.string().required(),
      email: Joi.string().email().required()
    }).options({ stripUnknown: true })

    const { error, value } = schema.validate(req.body, {
      abortEarly: false
    })

    if (error) {
      const message = error.details.map((d) => d.message).join(', ')
      res.status(400).json({ success: false, message })
    }

    const userParams = value
    const userCreated = await userService.createUser(userParams)

    res.status(201).json({
      success: true,
      message: 'User created successfully',
      user: {
        name: userCreated.name,
        email: userCreated.email
      }
    })
  } catch (error) {
    return res.status(500).json({
      success: false,
      message: 'Error creating user',
      error: error.message
    })
  }
}

const signIn = async (req, res) => {
  try {
    const schema = Joi.object({
      email: Joi.string().email().required(),
      password: Joi.string().required()
    }).options({ stripUnknown: true })

    const { error, value } = schema.validate(req.body, {
      abortEarly: false
    })

    if (error) {
      const message = error.details.map((d) => d.message).join(', ')
      res.status(400).json({ success: false, message })
    }

    const userParams = value
    const signInResult = await userService.authenticateUser(userParams)

    if (signInResult.success) {
      res.setHeader('Set-Cookie', signInResult.serializedToken)
      return res.status(200).json({
        success: true,
        message: 'Login successful',
        data: signInResult.serializedToken
      })
    } else {
      res.json({
        success: false,
        message: signInResult.message
      })
    }
  } catch (error) {
    return res.status(500).json({ success: false, message: error.message })
  }
}

module.exports = { signUp, signIn }
