# Tv Series Tracker


This project uses Node.js and Sequelize allows a registered user to search for series, search for TV series, manage their list together with the possibility of seeing the episodes of said series and marking them as seen.


## Tech info
* Node.js v18.14.0 [mote info](https://nodejs.org/en)
* Sequelize To interact with MySQL database [mote info](https://sequelize.org/)
* ESLint [mote info](https://eslint.org/docs/latest/use getting-started)
* JSON Web Token implementation [mote info](https://jwt.io/)
* Use of *DOT notation* for organization of related files in a project
* Multi-layer architecture approach
* Simple implementation of the Facade pattern for export and imoport modules


## Setup

### Environment Variables

Copy the following environment variables and replace `your-value` with the appropriate values:

``` 
#exmaple
IMDB_API_KEY=apikey
IMDB_BASE_URL=https://imdb-api.com/en-US/API
PORT=4000
NODE_ENV=development
DB_USERNAME=dbusername
DB_PASSWORD=password
DB_DATABASE=databasename
DB_HOST=mysql-cluster-fake.example.com
DB_PORT=36060
AUTH_JWT_SECRET=secret
AUTH_JWT_EXPIRES=2h
AUTH_JWT_ROUNDS=10
```

###  Dependency Installation
Run in your terminal
``` bash
  npm install
```


## Run API

Run in your terminal
``` bash
  npm run dev
```


## API Endpoints

### 1. Sign In

Authenticates the user

**Request Parameters:**
- password: string (required)
- email: string (required)

**Example Request:**

```
POST /api/auth/signin
Content-Type: application/json
```
``` JSON

{
    "password": "123456",
    "email": "email@domain.com"
}
```


> **Note:** this api withdraws the token via cookies  example: 'Cookie: tokenCook=token
 

**Example Response:**
``` JSON
{
    "success": true,
    "message": "Login successful"
}
```

### 2. Search TV Series by Keyword

Returns TV series by keyword.

**Request Parameters:**
- keyword: string (required)

**Example Request:**

```
GET /api/series/search?keyword=fringe
Cookie: tokenCook=<value_of_token>
```

**Example Response:**
``` JSON
{
    "success": true,
    "data": [
        {
            "id": "tt1119644",
            "resultType": "Series",
            "image": "https://path/image/name.jpg",
            "title": "Fringe",
            "description": "2008–2013 TV Series Anna Torv, Joshua Jackson"
        },
        {
          "id": "tt12306964",
          "resultType": "Series",
          "image": "https://path/image/name.jpg,
          "title": "On the Fringe",
          "description": "2022 Penélope Cruz, Luis Tosar"
        }
    ]
}
```

### 3. User's TV Series List related to logged in user


Returns the strings of the logged in user

```
GET /api/user-series
Cookie: tokenCook=<value_of_token>
```

**Example Response:**
``` JSON
{
  "success": true,
  "data": [
    {
      "id": 1,
      "user_id": 1,
      "serie_id": 2,
      "createdAt": "2023-04-27T17:57:51.000Z",
      "updatedAt": "2023-04-27T17:57:51.000Z",
      "Serie": {
        "id": 2,
        "code": "tt1119644",
        "image": "https://path/image/name.jpg",
        "title": "Fringe",
        "resultType": "Series",
        "description": "2008–2013 TV Series Anna Torv, Joshua Jackson",
        "seasons": [
          "1",
          "2",
          "3",
          "4",
          "5"
        ],
        "createdAt": "2023-04-27T17:57:51.000Z",
        "updatedAt": "2023-04-27T17:57:51.000Z"
      }
    }]
```



## In progress:
1. Finish user-series api logic to show the user the chapters and can keep track of which ones are seen or not
## Pending
1. Add/Remove show
2. Enable episode to be marked as watched.